# README #


### What is this repository for? ###

This is my interpretation of the [Sandi Metz talk about the Gilded Rose kata](https://youtu.be/8bZh5LMaSmE).

I modified the source to fit the code she uses in her talk.

Since the source repository I had didn't have any tests, I wrote them myself along the original guidelines listed in [Jim Weirich's example](https://github.com/jimweirich/gilded_rose_kata#original-description-of-the-gilded-rose).

Each commit in the sandi_metz_solution branch should correspond with one step in her talk.

Please be aware, that some of my code differs from the examples seen in the talk, because they don't handle certain edge cases like quality that could go below 0 or over 50.

### How do I get set up? ###

* Clone this repository
* Run: `bundler install`
* Switch to the sandi_metz_solution branch: `git checkout sandi_metz_solution`
* Run the tests: `./test_gilded_rose.rb`